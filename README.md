# data_intensive_Barcelona_project

Il progetto consiste nella valutazione di differenti insiemi di variabili per vedere con quale tasso di errore � possibile prevedere il tasso di disocuppazione della citt� di Barcellona.

Per l'esecuzione del progetto � necessario aver scaricato i data set utilizzati al suo interno.
Questi ultimi possono essere scaricati da questo stesso repository, o dal seguente link:
https://www.kaggle.com/xvivancos/barcelona-data-sets

